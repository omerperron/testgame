﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	private Vector3 direction;
	private float velocity;

	void Start () {
		direction = new Vector3 (0, 0, 1);
		velocity = 30;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += direction * velocity * Time.deltaTime;
		if (transform.position.z > Game.getTopBorder ()) {
			Game.removeBullet (this.gameObject);
		}
	}


	void OnTriggerEnter(Collider other){
		if (other.tag == "asteroid") {
			Game.removeBullet (this.gameObject);
			Game.removeAsteroid (other.gameObject);
			Game.incrementScore ();
		}
	}

}
