﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShip : MonoBehaviour {

	private Vector3 lastTouchPos; //for Input.touch delta time

	private Vector3 position; //so we can edit individual components
	private Vector3 wantedPosition;

	[SerializeField] float rateOfFire = 2f;
	[SerializeField] float speed = 5f;


	void Start () {
		position = transform.position;
		wantedPosition = position;
		lastTouchPos = new Vector3 (); //invalid value
	}

	void Update () {
		if (Time.timeSinceLevelLoad % (1f/rateOfFire) < Time.deltaTime)
			shoot ();

		Vector3 change = getChangeInInput ();
		change = new Vector3 (change.x * Game.worldWidth, 0, change.z * Game.worldHeight);
		wantedPosition += change;
		position = Vector3.Lerp (position, wantedPosition, Time.deltaTime * speed);
		clampToScreen ();
		transform.position = position;

	}



	private void clampToScreen(){
		if (position.x < Game.getLeftBorder ()) position.x = Game.getLeftBorder ();
		if (position.x > Game.getRightBorder ()) position.x = Game.getRightBorder ();
		if (position.z > Game.getTopBorder ()) position.z = Game.getTopBorder ();
		if (position.z < Game.getBottomBorder ()) position.z = Game.getBottomBorder ();
	}
		

	//returns change in finger location on phone or mouse position on laptop
	Vector3 getChangeInInput(){
		Vector2 delta = new Vector2 ();

		if (Input.touches.Length > 0) {
			Touch t = Input.GetTouch (0);
			delta = t.deltaPosition;

		} else if (Input.GetMouseButton (0)) {
			if (Input.GetMouseButtonDown(0)) lastTouchPos = Input.mousePosition;
			delta = Input.mousePosition - lastTouchPos;
			lastTouchPos = Input.mousePosition;
		}
		return new Vector3(delta.x/Screen.width, 0, delta.y / Screen.height);

	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "asteroid") {
			Game.restart ();
		}
	}


	private void shoot(){
		Game.createBullet (transform.position);
	}


}
