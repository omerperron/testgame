﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {

	private Quaternion rotation;
	private Vector3 direction;
	private float speed;


	void Start(){
		this.direction = new Vector3 (0, 0, -1);
		this.speed = 10;
		this.rotation.eulerAngles = new Vector3 (Random.value-1, Random.value-1, Random.value-1) * 100;

	}
	
	void Update () {
		transform.position += direction * speed * Time.deltaTime;
		transform.rotation = Quaternion.Slerp(transform.rotation, transform.rotation * this.rotation, Time.deltaTime);

		if (transform.position.z < Game.getBottomBorder ()) {
			Game.removeAsteroid (this.gameObject);
		}
	}



}
