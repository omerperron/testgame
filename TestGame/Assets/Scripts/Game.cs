﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {


	public static Vector3 screenBL;
	public static float worldWidth;
	public static float worldHeight;

	private static List<GameObject> asteroidPool;
	private static List<GameObject> bulletPool;

	private static int gameScore = 0;

	[SerializeField] Text score;
	[SerializeField] Text time;


	// Use this for initialization
	void Start () {
		Game.screenBL = Game.getWorldPos (0, 0);
		Game.worldWidth = Game.getWorldPos(1, 0).x - Game.screenBL.x;
		Game.worldHeight = Game.getWorldPos(0,1).z - Game.screenBL.z;
		Game.bulletPool = new List<GameObject> ();
		Game.asteroidPool = new List<GameObject> ();

	}

	void Update(){

		time.text = Time.timeSinceLevelLoad.ToString ("00.00");
		score.text = Game.gameScore.ToString ();
	}


	public static GameObject createBullet(Vector3 position){
		GameObject bullet;
		if (Game.bulletPool.Count == 0) {
			bullet = Instantiate ((GameObject)Resources.Load ("Bullet"), position, Quaternion.identity) as GameObject;
		} else {
			bullet = Game.bulletPool [0];
			bullet.SetActive (true);
			bullet.transform.position = position;
			Game.bulletPool.RemoveAt (0);
		}	
		return bullet;

	}



	public static void removeBullet(GameObject bullet){
		bullet.SetActive (false);
		Game.bulletPool.Add (bullet);
	}

	public static GameObject createAsteroid(Vector3 position){
		GameObject asteroid;
		if (Game.asteroidPool.Count == 0) {
			string color = getColor ();

			asteroid = Instantiate ((GameObject)Resources.Load (color), position, Quaternion.identity) as GameObject;
		} else {
			asteroid = Game.asteroidPool [0];
			asteroid.SetActive (true);
			asteroid.transform.position = position;
			Game.asteroidPool.RemoveAt (0);
		}	
		return asteroid;

	}

	private static string getColor(){
		switch (Random.Range (0, 4)) {
		case 0: 
			return "AsteroidBlue";
		case 1: 
			return "AsteroidRed";
		case 2: 
			return "AsteroidGreen";
		case 3: 
			return "AsteroidViolet";
		default: 
			return "AsteroidRed";
		}
	}

	public static void removeAsteroid(GameObject asteroid){
		asteroid.SetActive (false);
		Game.asteroidPool.Add (asteroid);
	}

	public static void incrementScore(){
		Game.gameScore++;
	}

	public static void resetScore(){
		Game.gameScore = 0;
	}

	public static void restart(){
		resetScore ();
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);

	}
		
	//gets world pos given view port coordinaes
	public static Vector3 getWorldPos(float x, float y){
		return Camera.main.ViewportToWorldPoint(new Vector3(x, y, Camera.main.farClipPlane));	
	}

	public static float getLeftBorder(){
		return Game.screenBL.x;
	}
	public static float getRightBorder(){
		return Game.screenBL.x + Game.worldWidth;
	}
	public static float getTopBorder(){
		return Game.screenBL.z + Game.worldHeight;
	}
	public static float getBottomBorder(){
		return Game.screenBL.z;
	}



}
