﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGenerator : MonoBehaviour {

	[SerializeField] float rate;


	// Update is called once per frame
	void Update () {
		if (Time.timeSinceLevelLoad % (1f/rate) < Time.deltaTime)
			generateAsteroid ();

		if (Time.timeSinceLevelLoad % 5 < Time.deltaTime)
			rate += 0.2f;
		
	
	}

	float getRandomXPosition(){
		return Random.value;
	}

	private void generateAsteroid(){
		Vector3 pos = Game.getWorldPos (getRandomXPosition(), 1);
		GameObject a = Game.createAsteroid(pos);
			
	}




}
